## RecBox Xfce shortcuts
Shortcuts list for XFCE desktop environment.

![shortcuts](https://gitlab.com/recbox/recbox-screenshots/-/raw/main/recbox-xfce-shortcuts.png)

## Installation:

```
git clone https://gitlab.com/recbox/recbox-tools-standalone/recbox-xfce-shortcuts.git
cd recbox-xfce-shortcuts
sudo cp -r sudo cp -r usr/* /usr/
sudo gtk-update-icon-cache -f /usr/share/icons/hicolor/
pamac install zenity
```

## Translation
Directory containing translations can be found here:
* [**/usr/share/xfce-shortcuts**](https://gitlab.com/recbox/recbox-xfce/recbox-scripts/-/tree/master/usr/share/xfce-shortcuts)
- [x] de_DE
- [x] en_US
- [x] fr_FR
- [x] pl_PL

## Translation contribution:
* type `echo $LANG` in terminal to check language used by your system
* copy `en_US.trans` file and rename it (replace `.utf8` with `.trans`)
* replace text under quotation marks
